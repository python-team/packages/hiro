Source: hiro
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Nicolas Dandrimont <olasd@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 dh-sequence-sphinxdoc,
 furo <!nodoc>,
 pybuild-plugin-pyproject,
 python3-all,
 python3-doc <!nodoc>,
 python3-pytest <!nocheck>,
 python3-pytest-cov <!nocheck>,
 python3-setuptools,
 python3-sphinx <!nodoc>,
 python3-sphinx-autobuild <!nodoc>,
 python3-sphinx-copybutton <!nodoc>,
 python3-sphinx-paramlinks <!nodoc>,
 python3-sphinxext-opengraph <!nodoc>,
Rules-Requires-Root: no
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/python-team/packages/hiro.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/hiro
Homepage: https://github.com/alisaifee/hiro

Package: python-hiro-doc
Architecture: all
Section: doc
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Multi-Arch: foreign
Description: time manipulation utilities for Python - documentation
 The hiro module provides a context-manager which hijacks a few commonly used
 time function to manipulate time in its context. It allows you to rewind,
 forward, freeze, unfreeze, and scale time according to given settings.
 .
 Most notably, the builtin functions time.sleep, time.time, time.gmtime,
 datetime.now, datetime.utcnow and datetime.today behave according the
 configuration of the context.
 .
 This package provides the documentation of the hiro module.

Package: python3-hiro
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Recommends:
 ${python3:Recommends},
Suggests:
 python-hiro-doc,
 ${python3:Suggests},
Description: time manipulation utilities for Python
 The hiro module provides a context-manager which hijacks a few commonly used
 time function to manipulate time in its context. It allows you to rewind,
 forward, freeze, unfreeze, and scale time according to given settings.
 .
 Most notably, the builtin functions time.sleep, time.time, time.gmtime,
 datetime.now, datetime.utcnow and datetime.today behave according the
 configuration of the context.
 .
 This package provides the Python 3 version of the hiro module.
